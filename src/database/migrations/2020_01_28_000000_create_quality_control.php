<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQualityControl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up() : void
    {
        try{
            Schema::create('quality_control', static function (Blueprint $table) {
                $table->increments('id');
                $table->uuid('uuid');
                $table->timestamps();
                $table->integer('user_id')->unsigned();
                $table->string('number');
                $table->string('order_id');
                $table->string('order_number');
                $table->dateTime('order_deadline');
                $table->string('order_name');
                $table->string('customer');
                $table->string('binding_type');
                $table->integer('error_place');
                $table->integer('qty_total');
                $table->integer('qty_done');
                $table->integer('qty_broken');
                $table->integer('qty_undone');
                $table->integer('qty_missing');
                $table->softDeletes();

                $table->foreign('user_id')->references('id')->on('users')
                    ->onUpdate('no action')->onDelete('no action');
            });
        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }

        Artisan::call('db:seed', [
            '--class' => \Totem\SamQualityControl\Database\Seeds\PermissionSeeder::class
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down() : void
    {
        Schema::dropIfExists('quality_control');

        (new \Totem\SamQualityControl\Database\Seeds\PermissionSeeder)->down();
    }
}
