<?php

use Faker\Generator as Faker;
use Totem\SamUsers\App\Model\User;

/**
 * @var $factory \Illuminate\Database\Eloquent\Factory
 */
$factory->define(\Totem\SamQualityControl\App\Model\Submission::class, static function (Faker $faker) {

    return [
        'user_id'           => User::inRandomOrder()->get()->first()->id,
        'order_id'          => $faker->randomNumber(5),
        'order_number'      => $faker->numerify('ZL/######/####'),
        'order_deadline'    => $faker->dateTime,
        'order_name'        => $faker->sentence(5),
        'customer'          => $faker->name,
        'binding_type'      => $faker->randomElement(\Rudashi\Profis\Enums\BindType::toArray()),
        'error_place'       => $faker->randomElement(\Totem\SamErrorsTypes\App\Enums\ErrorPlaceType::toArray()),
        'qty_total'         => $faker->numberBetween(100, 300),
        'qty_done'          => $faker->numberBetween(50, 100),
        'qty_broken'        => $faker->numberBetween(10, 30),
        'qty_undone'        => $faker->numberBetween(10, 50),
        'qty_missing'       => $faker->numberBetween(10, 20),
    ];
});