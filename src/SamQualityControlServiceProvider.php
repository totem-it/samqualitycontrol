<?php

namespace Totem\SamQualityControl;

use Totem\SamCore\App\Traits\TraitServiceProvider;
use Illuminate\Support\ServiceProvider;
use Totem\SamQualityControl\App\Model\Submission;
use Totem\SamQualityControl\App\Observers\SubmissionObserver;

class SamQualityControlServiceProvider extends ServiceProvider
{
    use TraitServiceProvider;

    public function getNamespace(): string
    {
        return 'sam-quality-control';
    }

    public function boot(): void
    {
        $this->loadJsonTranslationsFrom(__DIR__ . '/resources/lang');
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');

        $this->publishes([
            __DIR__ . '/resources/lang' => resource_path('lang/vendor/' . $this->getNamespace()),
        ], $this->getNamespace() . '-lang');
        $this->publishes([
            __DIR__ . '/database/migrations' => database_path('migrations'),
        ], $this->getNamespace() . '-migrations');

        if ($this->isWebProject()) {
            $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        }
        $this->loadRoutesFrom(__DIR__ . '/routes/api.php');

        Submission::observe(SubmissionObserver::class);
    }

    public function register(): void
    {
        $this->registerEloquentFactoriesFrom(__DIR__. '/database/factories');

        $this->app->bind(
            \Totem\SamQualityControl\App\Repositories\Contracts\SubmissionsRepositoryInterface::class,
            \Totem\SamQualityControl\App\Repositories\SubmissionsRepository::class
        );
    }

}
