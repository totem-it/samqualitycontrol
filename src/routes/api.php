<?php

Route::group(['prefix' => 'api' ], static function() {

    Route::middleware(config('sam-admin.guard-api'))->group(static function() {

        Route::group(['prefix' => 'qc'], static function() {
            Route::middleware('permission:quality-control.view')->group(static function() {

                Route::get('/', 'Totem\SamQualityControl\App\Controllers\ApiSubmissionsController@index')->name('api.quality-control.index');

            });
        });

    });

});