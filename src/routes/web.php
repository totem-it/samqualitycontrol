<?php

Route::group(['middleware' => ['web']], static function () {

    Route::group(['prefix' => config('sam-admin.admin-route-prefix')], static function () {

        Route::group(['middleware' => ['auth:' . config('sam-admin.guard')]], static function () {

            Route::group(['prefix' => 'qc', 'middleware' => 'permission:quality-control.view'], static function () {

            });

        });
    });
});