<?php

namespace Totem\SamQualityControl\App\Repositories\Contracts;

use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;

interface SubmissionsRepositoryInterface extends RepositoryInterface
{

}