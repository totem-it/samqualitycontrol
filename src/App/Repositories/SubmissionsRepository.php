<?php

namespace Totem\SamQualityControl\App\Repositories;

use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamQualityControl\App\Model\Submission;
use Totem\SamQualityControl\App\Repositories\Contracts\SubmissionsRepositoryInterface;

class SubmissionsRepository extends BaseRepository implements SubmissionsRepositoryInterface
{

    public function model(): string
    {
        return Submission::class;
    }

}