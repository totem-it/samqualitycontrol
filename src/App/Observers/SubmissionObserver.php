<?php

namespace Totem\SamQualityControl\App\Observers;

use Totem\SamQualityControl\App\Model\Submission;

class SubmissionObserver
{

    public function creating(Submission $submission): void
    {
        $submission->number = $submission->createNumber();
    }

}