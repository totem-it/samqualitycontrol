<?php

namespace Totem\SamQualityControl\App\Controllers;

use Totem\SamQualityControl\App\Repositories\Contracts\SubmissionsRepositoryInterface;
use Totem\SamCore\App\Controllers\ApiController;

class ApiSubmissionsController extends ApiController
{

    public function __construct(SubmissionsRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {

    }

}