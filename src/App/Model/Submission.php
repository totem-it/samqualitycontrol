<?php

namespace Totem\SamQualityControl\App\Model;

use Totem\SamCore\App\Traits\HasUuid;
use Illuminate\Database\Eloquent\SoftDeletes;
use Totem\SamMessenger\App\Traits\HasMessenger;
use Totem\SamCore\App\Traits\GetNextSequenceValue;
use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    use SoftDeletes,
        HasMessenger,
        HasUuid,
        GetNextSequenceValue;

    public const PREFIX = 'ITS';
    public const NUMBER_PAD = 5;

    protected $casts = [
        'qty_total' => 'int',
    ];

    public function __construct(array $attributes = [])
    {
        $this->setTable('quality_control');
        $this->setKeyName('uuid');

        $this->fillable([
            'user_id',
            'number',
            'order_id',
            'order_number',
            'order_deadline',
            'order_name',
            'customer',
            'binding_type',
            'error_place',
            'qty_total',
            'qty_done',
            'qty_broken',
            'qty_undone',
            'qty_missing',
        ]);

        parent::__construct($attributes);
    }

    public function createNumber(): string
    {
        return self::PREFIX . DIRECTORY_SEPARATOR . str_pad(self::getNextSequenceValue(), self::NUMBER_PAD, 0, STR_PAD_LEFT) . DIRECTORY_SEPARATOR . date('Y');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(app('config')->get('auth.providers.users.model'))->withTrashed();
    }
}