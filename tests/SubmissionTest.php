<?php

namespace Totem\SamQualityControl\Tests;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Totem\SamQualityControl\App\Model\Submission;
use Totem\SamQualityControl\App\Repositories\SubmissionsRepository;

class SubmissionTest extends \Tests\TestCase
{
    use DatabaseTransactions;

    private $repository;

    public function testGetAllSubmissions() : void
    {
        factory(Submission::class)->create();

        $repository = new SubmissionsRepository();
        $result = $repository->all();

        $this->assertInstanceOf(Submission::class, $result->first());
        $this->assertNotSame('dupa', $result->first()->number);
    }

}