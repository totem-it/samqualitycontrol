Sam Quality Control Component
================

This package manage internal complaints.

![Totem.com.pl](https://www.totem.com.pl/files/totem.png)

General System Requirements
-------------
- [PHP >7.1.0](http://php.net/)
- [Laravel ~6.*](https://github.com/laravel/framework)
- [SAM-core ~1.*](https://bitbucket.org/rudashi/samcore)  
- [SAM-Admin ~1.*](https://bitbucket.org/rudashi/samadmin)
- [SAM-Users ~1.*](https://bitbucket.org/rudashi/samusers)  


Quick Installation
-------------
If needed use composer to grab the library

```
$ composer require totem-it/sam-quality-control
```

```
"repositories": [
    {
        "type": "vcs",
        "url":  "https://bitbucket.org/totem-it/samacl.git"
    }
],
```

Usage
-------------


Authors
-------------

* **Borys Zmuda** - Lead designer - [GoldenLine](http://www.goldenline.pl/borys-zmuda/)